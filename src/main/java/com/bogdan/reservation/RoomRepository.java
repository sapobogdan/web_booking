package com.bogdan.reservation;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.lang.Nullable;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

@Repository
public class RoomRepository {

    @Autowired
    private JdbcTemplate jdbcTemplate;

    public List<Room> getAllRooms() {
        return jdbcTemplate.query("select * from rooms", new RowMapper<Room>() {

            @Override
            public Room mapRow(ResultSet resultSet, int i) throws SQLException {
                Room room = new Room();
                room.setId(resultSet.getInt("id"));
                room.setTitle(resultSet.getString("title"));
                room.setDescription(resultSet.getString("description"));

                return room;

            }
        });
    }
}
