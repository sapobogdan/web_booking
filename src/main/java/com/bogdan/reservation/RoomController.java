package com.bogdan.reservation;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import java.util.List;

@Controller
public class RoomController {

    @Autowired
    private RoomRepository roomRepository;


    @RequestMapping("/rooms")
    public ModelAndView getRooms() {
        ModelAndView modelAndView = new ModelAndView("roomsView");
        modelAndView.addObject("rooms", roomRepository.getAllRooms());

        return modelAndView;
    }
}
